var requestURL = 'assets/chart.json';
var request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function () {
    var chart = request.response;
    populateProducts(chart);
    goDetails(chart);
}


/* Products Section */


var divElement;
function populateProducts(jsonObj) {
    for (var i = 0; i < jsonObj['products'].length; i++) {
        divElement = '';
        divElement += '<div class="col-md-4" >';
        divElement += '<img src="' + jsonObj['products'][i].productUrl + '" >';
        divElement += '<a href="shoppingChart.html?productId=' + i + '"><button  class="btn btn-default" onclick="goDetails()" > Go Details </button></a>';
        divElement += '</div';
        $("#productsRow").append(divElement);

    }
}
function goDetails(jsonObj) {
    var query = window.location.search.substring(1);
    var vars = query.split("=");
    var id = vars[1];
    
   
    document.getElementById("image").src = jsonObj['products'][id].productUrl;
    document.getElementById("name").innerHTML = jsonObj['products'][id].productName;
    document.getElementById("price").innerHTML = jsonObj['products'][id].productPrice;
    document.getElementById("quantity").innerHTML = jsonObj['products'][id].productQuantity;
}
