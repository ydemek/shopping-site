/* Slider Section */
var currentIndex = 0,
    items = $('#container div');
    itemAmt = items.length;
function cycleItems(){
    var item = $('#container div').eq(currentIndex);
    items.hide();
    item.css('display','inline-block');
}

var autoSlide = setInterval(function(){
    currentIndex += 1;
    if(currentIndex > itemAmt - 1){
        currentIndex = 0;
    }
    cycleItems();
}, 3000);

$('.next').click(function() {
    clearInterval(autoSlide);
    currentIndex += 1;
    if(currentIndex > itemAmt - 1){
        currentIndex = 0;
    }
    cycleItems();
});
$('.prev').click(function() {
    clearInterval(autoSlide);
    currentIndex -= 1;
    if(currentIndex < 0){
        currentIndex = itemAmt - 1;
    }
    cycleItems();
});



    var requestURL = 'assets/chart.json';
    var request = new XMLHttpRequest();
    request.open('GET', requestURL);
    request.responseType = 'json';
    request.send();
    request.onload = function () {
      var chart = request.response;
       populateSlide(chart);
       populateDiscount(chart);
     
    }
    

function populateSlide(jsonObj){

        document.getElementById("0").src = jsonObj['slider'][0].chartId ;
        document.getElementById("1").src = jsonObj['slider'][1].chartId ;
        document.getElementById("2").src = jsonObj['slider'][2].chartId ;
        document.getElementById("3").src = jsonObj['slider'][3].chartId ;
        document.getElementById("4").src = jsonObj['slider'][4].chartId ;
        
}

/* Discount Section */

function populateDiscount(jsonObj){
    for(var i = 5; i < 8; i++){
        document.getElementById(i).src = jsonObj['products'][i-5].productUrl;
    }
}



